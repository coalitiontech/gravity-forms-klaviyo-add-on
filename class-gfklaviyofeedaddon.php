<?php

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

GFForms::include_feed_addon_framework();

class GFKlaviyoAPI extends GFFeedAddOn {

	protected $_version                  = GF_KLAVIYO_API_VERSION;
	protected $_min_gravityforms_version = '1.19.12';
	protected $_slug                     = 'klaviyoaddon';
	protected $_path                     = 'klaviyoaddon/klaviyoaddon.php';
	protected $_full_path                = __FILE__;
	protected $_title                    = 'Gravity Forms Klaviyo Feed Add-On';
	protected $_short_title              = 'Klaviyo';

	private static $_instance = null;

	/**
	 * Get an instance of this class.
	 *
	 * @return GFKlaviyoAPI
	 */
	public static function get_instance() {
		if ( self::$_instance == null ) {
			self::$_instance = new GFKlaviyoAPI();
		}

		return self::$_instance;
	}

	/**
	 * Plugin starting point. Handles hooks, loading of language files and PayPal delayed payment support.
	 */
	public function init() {

		parent::init();

		$this->add_delayed_payment_support(
			array(
				'option_label' => 'Subscribe contact to Klaviyo only when payment is received.',
			)
		);

	}

	/**
	 * Enqueues required styleseheets
	 *
	 * Defines required styles for the Klaviyo add-on, and adds them to styles in GFFeedAddOn
	 *
	 * @see GFFeedAddOn::styles()
	 *
	 * @return array Contains the styles to be enqueued
	 */
	public function styles() {

		$styles = array(
			array(
				'handle'  => 'gform_ct_klaviyo_widget_editor',
				'src'     => $this->get_base_url() . '/css/editor.css',
				'version' => $this->_version,
				'enqueue' => array(
					array( $this, 'can_enqueue_widget_editor_script' ),
					array( 'admin_page' => array( 'customizer' ) ),
				),
			),
		);

		return array_merge( parent::styles(), $styles );

	}

	/**
	 * Return the plugin's icon for the plugin/form settings menu.
	 *
	 * @since 4.5
	 *
	 * @return string
	 */
	public function get_menu_icon() {

		return 'gform-icon--ct-klaviyo';

	}


	// # FEED PROCESSING -----------------------------------------------------------------------------------------------

	/**
	 * Process the feed e.g. subscribe the user to a list.
	 *
	 * @param array $feed The feed object to be processed.
	 * @param array $entry The entry object currently being processed.
	 * @param array $form The form object currently being processed.
	 *
	 * @return bool|void
	 */
	public function process_feed( $feed, $entry, $form ) {
		$feedName = $feed['meta']['feedName'];
		$list_id  = $feed['meta']['list'];

		// Retrieve the name => value pairs for all fields mapped in the 'mappedFields' field map.
		$field_map = $this->get_field_map_fields( $feed, 'mappedFields' );

		// Loop through the fields from the field map setting building an array of values to be passed to the third-party service.
		$merge_vars = array();
		foreach ( $field_map as $name => $field_id ) {

			// Get the field value for the specified field id
			$merge_vars[ $name ] = $this->get_field_value( $form, $entry, $field_id );

		}

		// Send the values to the third-party service.
		if ( $this->get_plugin_setting( 'api_key' ) ) {
			$tracker = new Klaviyo( $this->get_plugin_setting( 'api_key' ) );

			$properties = array(
				'$email'        => $merge_vars['email'],
				'$first_name'   => $merge_vars['first_name'],
				'$last_name'    => $merge_vars['last_name'],
				'$phone_number' => $merge_vars['phone_number'],
				'$organization' => $merge_vars['organization'],
				'$title'        => $merge_vars['title'],
				'$city'         => $merge_vars['city'],
				'$region'       => $merge_vars['region'],
				'$zip'          => $merge_vars['zip'],
				'$country'      => $merge_vars['country'],
				'$timezone'     => $merge_vars['timezone'],
				'Opted in?'     => $merge_vars['Opted in?'],
			);

			$properties = array_merge( $properties, $merge_vars );

			$tracker->track(
				'Active on Site',
				$properties
				// array( 'Item SKU' => 'ABC123', 'Payment Method' => 'Credit Card' ),
				// 1354913220
			);
		}

		if ( $this->get_plugin_setting( 'private_api_key' ) ) {

			$url = 'https://a.klaviyo.com/api/v2/list/' . $list_id . '/members';

			$post_data = array(
				'api_key'  => $this->get_plugin_setting( 'private_api_key' ),
				'profiles' => array(
					array(
						'email'    => $merge_vars['email'],
						'$consent' => 'email',
						'Source'   => 'Gravity Forms: ' . $form['title'],
					),
				),
			);

			if ( isset( $merge_vars['first_name'] ) ) {
				$post_data['profiles'][0]['$first_name'] = $merge_vars['first_name'];
			}

			if ( isset( $merge_vars['last_name'] ) ) {
				$post_data['profiles'][0]['$last_name'] = $merge_vars['last_name'];
			}

			if ( isset( $merge_vars['phone_number'] ) ) {
				$post_data['profiles'][0]['$phone_number'] = $merge_vars['phone_number'];
			}

			if ( isset( $merge_vars['organization'] ) ) {
				$post_data['profiles'][0]['$organization'] = $merge_vars['organization'];
			}

			if ( isset( $merge_vars['title'] ) ) {
				$post_data['profiles'][0]['$title'] = $merge_vars['title'];
			}

			if ( isset( $merge_vars['city'] ) ) {
				$post_data['profiles'][0]['$city'] = $merge_vars['city'];
			}

			if ( isset( $merge_vars['region'] ) ) {
				$post_data['profiles'][0]['$region'] = $merge_vars['region'];
			}

			if ( isset( $merge_vars['zip'] ) ) {
				$post_data['profiles'][0]['$zip'] = $merge_vars['zip'];
			}

			if ( isset( $merge_vars['country'] ) ) {
				$post_data['profiles'][0]['$country'] = $merge_vars['country'];
			}

			if ( isset( $merge_vars['timezone'] ) ) {
				$post_data['profiles'][0]['$timezone'] = $merge_vars['timezone'];
			}

			if ( isset( $merge_vars['Opted in?'] ) ) {
				if ( ! empty( $merge_vars['Opted in?'] ) ) {
					$post_data['profiles'][0]['Opted in?'] = 'Has opted in for email marketing';
				} else {
					$post_data['profiles'][0]['Opted in?'] = 'Has opted out for email marketing';
				}
			}

			// Retrieve the name => value pairs for all fields mapped in the 'mappedFields' field map.
			$customerPropertiesItems = $this->get_dynamic_field_map_fields( $feed, 'customerProperties' );

			foreach ( $customerPropertiesItems as $name => $field_id ) {

				$field = GFFormsModel::get_field( $form, $field_id );

				/*
				 * Gives us a chance to write hooks to alter values, these hooks should return false if they modify the postDataValues array
				 */
				$value = apply_filters( 'ct_modify_field_value', $this->get_field_value( $form, $entry, $field_id ), $name, $field, $this, $feed, $entry, $form );

				if ( ! $value ) {
					continue;
				}

				$post_data['profiles'][0][ $name ] = $value;

			}

			$response = wp_safe_remote_post(
				$url,
				array(
					'method'  => 'POST',
					'headers' => array(
						'content-type' => 'application/json',
					),
					'body'    => json_encode( $post_data ),
				)
			);

			// If the Klaviyo API returns a code anything other than OK, log it!
			if ( $response['response']['code'] != 200 ) {
				$this->log_error( __METHOD__ . '(): Could not add user to mailing list' );
				$this->log_error( __METHOD__ . '(): response => ' . print_r( $response, true ) );
			}

			$this->log_debug( print_r( $post_data, true ) );
			$this->log_debug( print_r( $merge_vars, true ) );
		}
	}

	/**
	 * Custom format the phone type field values before they are returned by $this->get_field_value().
	 *
	 * @param array          $entry The Entry currently being processed.
	 * @param string         $field_id The ID of the Field currently being processed.
	 * @param GF_Field_Phone $field The Field currently being processed.
	 *
	 * @return string
	 */
	public function get_phone_field_value( $entry, $field_id, $field ) {

		// Get the field value from the Entry Object.
		$field_value = rgar( $entry, $field_id );

		// If there is a value and the field phoneFormat setting is set to standard reformat the value.
		if ( ! empty( $field_value ) && $field->phoneFormat == 'standard' && preg_match( '/^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$/', $field_value, $matches ) ) {
			$field_value = sprintf( '%s-%s-%s', $matches[1], $matches[2], $matches[3] );
		}

		return $field_value;
	}

	// # ADMIN FUNCTIONS -----------------------------------------------------------------------------------------------


	/**
	 * Configures the settings which should be rendered on the add-on settings tab.
	 *
	 * @return array
	 */
	public function plugin_settings_fields() {
		return array(
			array(
				'title'  => 'Insert your Klaviyo API keys below to connect. You can find them on your Klaviyo account page.',
				'fields' => array(
					array(
						'name'  => 'api_key',
						'label' => 'Public API Key',
						'type'  => 'text',
						'class' => 'small',
					),
					array(
						'name'  => 'private_api_key',
						'label' => 'Private API Key',
						'type'  => 'text',
						'class' => 'medium',
					),
				),
			),
		);
	}

	/**
	 * Configures the settings which should be rendered on the feed edit page in the Form Settings > Klaviyo area.
	 *
	 * @return array
	 */
	public function feed_settings_fields() {
		return array(
			array(
				'title'  => 'Klaviyo Feed Settings',
				'fields' => array(
					array(
						'label'   => 'Feed name',
						'type'    => 'text',
						'name'    => 'feedName',
						'class'   => 'small',
						'tooltip' => '<h6>Name</h6>Enter a feed name to uniquely identify this setup.',
					),
					array(
						'name'     => 'list',
						'label'    => 'Klaviyo List',
						'type'     => 'select',
						'required' => true,
						'choices'  => $this->lists_for_feed_setting(),
						'tooltip'  => '<h6>Klaviyo List</h6>Select which Klaviyo list this feed will add contacts to.',
					),
					array(
						'name'      => 'mappedFields',
						'label'     => 'Map Fields',
						'type'      => 'field_map',
						'field_map' => array(
							array(
								'name'       => 'email',
								'label'      => 'Email',
								'required'   => true,
								'field_type' => array( 'email', 'hidden' ),
							),
							array(
								'name'     => 'first_name',
								'label'    => 'First Name',
								'required' => false,
							),
							array(
								'name'     => 'last_name',
								'label'    => 'Last Name',
								'required' => false,
							),
							array(
								'name'     => 'phone_number',
								'label'    => 'Phone Number',
								'required' => false,
							),
							array(
								'name'     => 'organization',
								'label'    => 'Company Name',
								'required' => false,
							),
							array(
								'name'     => 'title',
								'label'    => 'Title at Company',
								'required' => false,
							),
							array(
								'name'     => 'city',
								'label'    => 'City',
								'required' => false,
							),
							array(
								'name'     => 'region',
								'label'    => 'State / Province',
								'required' => false,
							),
							array(
								'name'     => 'zip',
								'label'    => 'Zip / Postal Code',
								'required' => false,
							),
							array(
								'name'     => 'country',
								'label'    => 'Country',
								'required' => false,
							),
							array(
								'name'     => 'timezone',
								'label'    => 'Timezone',
								'required' => false,
							),
							array(
								'name'     => 'Opted in?',
								'label'    => 'Opted in?',
								'required' => false,
							),
						),
					),
					array(
						'name'    => 'customerProperties',
						'label'   => 'Customer Properties',
						'type'    => 'dynamic_field_map',
						'tooltip' => '<h6>Customer Properties</h6><p>For the "Key" field, set the label that you would like the property to be saved as.<br>For the value, select which field\'s value should be mapped to that value.</p>',
					),
					array(
						'name'           => 'condition',
						'label'          => 'Condition',
						'type'           => 'feed_condition',
						'checkbox_label' => 'Enable Condition',
						'instructions'   => 'Process this feed if',
					),
				),
			),
		);
	}

	/**
	 * Configures which columns should be displayed on the feed list page.
	 *
	 * @return array
	 */
	public function feed_list_columns() {
		return array(
			'feedName' => 'Name',
			'list'     => 'Klaviyo List',
		);
	}

	/**
	 * Format the value to be displayed in the mytextbox column.
	 *
	 * @param array $feed The feed being included in the feed list.
	 *
	 * @return string
	 */
	public function get_column_value_mytextbox( $feed ) {
		return '<b>' . rgars( $feed, 'meta/mytextbox' ) . '</b>';
	}

	/**
	 * Prevent feeds being listed or created if an api key isn't valid.
	 *
	 * @return bool
	 */
	public function can_create_feed() {

		// Get the plugin settings.
		$settings = $this->get_plugin_settings();

		// Access a specific setting e.g. an api key
		$key = rgar( $settings, 'apiKey' );

		return true;
	}

	public function lists_for_feed_setting() {
		$lists = array(
			array(
				'label' => '',
				'value' => '',
			),
		);

		/*
		 If Klaviyo API credentials are invalid, return the lists array. */
		// if ( ! $this->initialize_api() ) {
		// return $lists;
		// }

		$private_key = $this->get_plugin_setting( 'private_api_key' );

		if ( $private_key ) {
			$url      = 'https://a.klaviyo.com/api/v2/lists?api_key=' . $private_key;
			$response = wp_remote_get( $url );

			$data = json_decode( $response['body'] );

			/* Get available Klaviyo lists. */
			$ac_lists = $data;

			/* Add Klaviyo lists to array and return it. */
			$lists = array();
			foreach ( $ac_lists as $list ) {

				$lists[] = array(
					'label' => $list->list_name,
					'value' => $list->list_id,
				);

			}
		}

		return $lists;

	}

}
